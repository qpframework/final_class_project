# Time-stamp: <2021-05-03 20:32:05 daniel>
# Makefile

# Header file location
inc = \
 -Iarm-cm33/qv \
 -Istm32L5xx/STM32L5xx_LL_Driver \
 -Istm32L5xx \
 -ICMSIS \
 -ICMSIS/STM32L5


def := -DNDEBUG -DUSE_FULL_LL_DRIVER=1


# tools
gnu_arm := /usr/bin
cc := $(gnu_arm)/arm-none-eabi-gcc
as := $(gnu_arm)/arm-none-eabi-as
oc := $(gnu_arm)/arm-none-eabi-objcopy
remove := /bin/rm

# flags
cpu := -mcpu=cortex-m33
arch := 8
fpu := -mfpu=vfp
float_abi := -mfloat-abi=softfp

asflags := $(cpu) $(fpu)

cflags = \
  -g $(cpu) $(fpu) $(float_abi) -mthumb -Wall \
  -ffunction-sections -fdata-sections \
  -O1 $(inc) $(def)

ldflags = \
	-Tlinker.ld $(cpu) $(fpu) $(float_abi) -mthumb -specs=nosys.specs -specs=nano.specs -Wl,-Map,$(target).map,--cref

# Rules
%.d : %.c ; $(cc) -MM -MT $(@:.d=.o) $(cflags) $< > $@
%.o : %.s ; $(as) $(asflags) -c -o $@ $<
%.o : %.c ; $(cc) $(cflags) -c -o $@ $<


src =  # empty - will be filled by included modules
objs = # also empty
deps = # also empty

include qpc.mk
include arm-cm33/qv/module.mk
include bsp/module.mk
include src/module.mk
include stm32L5xx/module.mk

objs += $(src:.c=.o)
deps += $(src:.c=.d)

# Define the main target
target := app
stamp := src/time_stamp

all : $(target).bin

$(target).bin : $(target).elf
	$(oc) -O binary $< $@

$(target).elf : $(deps) $(objs)
	$(cc) $(cflags) -c -o $(stamp).o $(stamp).c
	$(cc) $(ldflags) -o $@ $(objs)

# Include the dependency files
include $(deps)  # add in dependents

flash : $(target).bin
	/home/daniel/dev/st/bin/st-prog -c port=swd -w $^ 0x08000000

.PHONY : headers objects
headers : ; @printf "%s\n" $(inc)
objects : ; @printf "%s\n" $(objs)
depends : ; @printf "%s\n" $(deps)

.PHONY : clean
clean : ; $(remove) --force $(objs) $(deps) *.map *.elf *.bin
