
#ifndef QF_PORT_H
#define QF_PORT_H

/* The maximum number of system clock tick rates */
#define QF_MAX_TICK_RATE        2U

#if (__ARM_ARCH == 8) /* Cortex-M23/M33 */

    /* The maximum number of active objects in the application */
    #define QF_MAX_ACTIVE       32U

    /* alternative interrupt disabling with PRIMASK */
    #define QF_PRIMASK_DISABLE() __asm volatile ("cpsid i")
    #define QF_PRIMASK_ENABLE()  __asm volatile ("cpsie i")

    /* interrupt disabling policy */
    #define QF_INT_DISABLE() __asm volatile (\
        "cpsid i\n" "msr BASEPRI,%0\n" "cpsie i" :: "r" (QF_BASEPRI) : )
    #define QF_INT_ENABLE()  __asm volatile (\
        "msr BASEPRI,%0" :: "r" (0) : )

    /* QF critical section entry/exit (unconditional interrupt disabling) */
    /*#define QF_CRIT_STAT_TYPE not defined */
    #define QF_CRIT_ENTRY(dummy) QF_INT_DISABLE()
    #define QF_CRIT_EXIT(dummy)  QF_INT_ENABLE()

    /* BASEPRI threshold for "QF-aware" interrupts */
    #define QF_BASEPRI           0x3F

    /* CMSIS threshold for "QF-aware" interrupts */
    #define QF_AWARE_ISR_CMSIS_PRI (QF_BASEPRI >> (8 - __NVIC_PRIO_BITS))

    /* Cortex-M3/M4/M7 provide the CLZ instruction for fast LOG2 */
    #define QF_LOG2(n_) ((uint_fast8_t)(32U - __builtin_clz((unsigned)(n_))))

#else /* unsupported */
#error "Unsupported ARM architecture for this port"
#endif

#define QF_CRIT_EXIT_NOP()      __asm volatile ("isb")

#include "qep_port.h" /* QEP port */

#include "qk_port.h"  /* QK preemptive kernel port */
#include "qf.h"       /* QF platform-independent public interface */

#endif  // QF_PORT_H


