#ifndef QK_PORT_H
#define QK_PORT_H

/* determination if the code executes in the ISR context */
#define QK_ISR_CONTEXT_() (QK_get_IPSR() != 0U)

__attribute__((always_inline))
static inline uint32_t QK_get_IPSR(void) {
    uint32_t regIPSR;
    __asm volatile ("mrs %0,ipsr" : "=r" (regIPSR));
    return regIPSR;
}

/* QK interrupt entry and exit */
#define QK_ISR_ENTRY() ((void)0)

#define QK_ISR_EXIT()  do {                                   \
    QF_INT_DISABLE();                                         \
    if (QK_sched_() != 0U) {                                  \
        *Q_UINT2PTR_CAST(uint32_t, 0xE000ED04U) = (1U << 28U);\
        QK_ARM_ERRATUM_838869();                              \
    }                                                         \
    QF_INT_ENABLE();                                          \
} while (false)

    /* The following macro implements the recommended workaround for the
    * ARM Erratum 838869. Specifically, the DSB
    * (memory barrier) instruction needs to be added before exiting an ISR.
    */
    #define QK_ARM_ERRATUM_838869() \
        __asm volatile ("dsb" ::: "memory")

/* initialization of the QK kernel */
#define QK_INIT() QK_init()
void QK_init(void);
void QK_thread_ret(void);

#include "qk.h" /* QK platform-independent public interface */

#endif /* QK_PORT_H */

