# Time-stamp: <2021-05-02 17:39:02 daniel>
# arm-cm33/qv/module.mk

# add source files
src += \
 arm-cm33/qv/qv_port.c

# add header directories
inc += \
 -Iarm-cm33/qv
