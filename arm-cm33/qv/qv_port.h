#ifndef QV_PORT_H
#define QV_PORT_H

#if (__ARM_ARCH == 8) /* Cortex-M23/M33 */

    /* macro to put the CPU to sleep inside QV_onIdle() */
    #define QV_CPU_SLEEP() do { \
        QF_PRIMASK_DISABLE();   \
        QF_INT_ENABLE();        \
        __asm volatile ("wfi"); \
        QF_PRIMASK_ENABLE();    \
    } while (false)

    /* initialization of the QV kernel for Cortex-M3/M4/M4F */
    #define QV_INIT() QV_init()
    void QV_init(void);

    /* The following macro implements the recommended workaround for the
    * ARM Erratum 838869. Specifically, for Cortex-M3/M4/M7 the DSB
    * (memory barrier) instruction needs to be added before exiting an ISR.
    * This macro should be inserted at the end of ISRs.
    */
    #define QV_ARM_ERRATUM_838869() \
        __asm volatile ("dsb 0xf" ::: "memory")

#else
#error "Sorry, unknown ARM architecture"
#endif

#include "qv.h" /* QV platform-independent public interface */

#endif /* QV_PORT_H */

