
#include "bsp.h"
#include "serial.h"


// ****************************************************
#define LED1_PIN                           LL_GPIO_PIN_7
#define LED1_GPIO_PORT                     GPIOC
#define LED1_GPIO_CLK_ENABLE()             LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOC)

#define LED2_PIN                           LL_GPIO_PIN_7
#define LED2_GPIO_PORT                     GPIOB
#define LED2_GPIO_CLK_ENABLE()             LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOB)

#define LED3_PIN                           LL_GPIO_PIN_9
#define LED3_GPIO_PORT                     GPIOA
#define LED3_GPIO_CLK_ENABLE()             LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA)

// Button is input on PC13
#define USER_BUTTON_GPIO_PORT              GPIOC
#define USER_BUTTON_PIN                    LL_GPIO_PIN_13
#define USER_BUTTON_GPIO_PUPD              LL_GPIO_PULL_NO
#define USER_BUTTON_GPIO_CLK_ENABLE()      LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOC)


// ********************************************************************
void my_assert(void)
{
  while(1)
  {
    // Sit and spin
  }
}

static GPIO_TypeDef * const Led_port[] = { LED1_GPIO_PORT, LED2_GPIO_PORT, LED3_GPIO_PORT };
static uint32_t const Led_pin[] = { LED1_PIN, LED2_PIN, LED3_PIN };

// ********************************************************************
void BSP_led_on(int led_id)
{
  MY_ASSERT(led_id >= 1);
  MY_ASSERT(led_id <= 3);
  --led_id;
  LL_GPIO_SetOutputPin(Led_port[led_id], Led_pin[led_id]);
}

// ********************************************************************
void BSP_led_off(int led_id)
{
  MY_ASSERT(led_id >= 1);
  MY_ASSERT(led_id <= 3);
  --led_id;
  LL_GPIO_ResetOutputPin(Led_port[led_id], Led_pin[led_id]);
}

// ********************************************************************
void BSP_led_toggle(int led_id)
{
  MY_ASSERT(led_id >= 1);
  MY_ASSERT(led_id <= 3);
  --led_id;
  LL_GPIO_TogglePin(Led_port[led_id], Led_pin[led_id]);
}


// ********************************************************************
int UserButton_Pressed(void)
{
  return LL_GPIO_IsInputPinSet(GPIOC, USER_BUTTON_PIN);
}


// ********************************************************************
/**
  * @brief  Configures User push-button in GPIO or EXTI Line Mode.
  * @param  None
  * @retval None
  */
static void UserButton_Init(void)
{
  /* Enable the BUTTON Clock */
  USER_BUTTON_GPIO_CLK_ENABLE();
  
  /* Configure GPIO for BUTTON */
  LL_GPIO_SetPinMode(USER_BUTTON_GPIO_PORT, USER_BUTTON_PIN, LL_GPIO_MODE_INPUT);
  LL_GPIO_SetPinPull(USER_BUTTON_GPIO_PORT, USER_BUTTON_PIN, USER_BUTTON_GPIO_PUPD);
  
}


// ********************************************************************
static void ConfigureLeds(void)
{
  /* Enable the LED GPIO Clock(s) */
  LED1_GPIO_CLK_ENABLE();
  LED2_GPIO_CLK_ENABLE();
  LED3_GPIO_CLK_ENABLE();

  /* Configure IO in output push-pull mode to drive external LED1 */
  LL_GPIO_SetPinMode(LED1_GPIO_PORT, LED1_PIN, LL_GPIO_MODE_OUTPUT);
  LL_GPIO_SetPinMode(LED2_GPIO_PORT, LED2_PIN, LL_GPIO_MODE_OUTPUT);
  LL_GPIO_SetPinMode(LED3_GPIO_PORT, LED3_PIN, LL_GPIO_MODE_OUTPUT);

}

// ********************************************************************
static void Configure_GPIO(void)
{
  ConfigureLeds();

  UserButton_Init();
}

// ********************************************************************
static void periph_clocks_enable_rng(void)
{
  // *******************************************************
  // ********   RNG clock setup                  ***********
  // *******************************************************

  // Enable HSI48 and wait for it to stabilize.
  // (see Reference Manual, 9.8.31)
  LL_RCC_HSI48_Enable();
  while (!(LL_RCC_HSI48_IsReady())) { }

  // Set HSI48 as clock for RNG (see Reference Manual 9.8.28)
  LL_RCC_SetRNGClockSource(LL_RCC_RNG_CLKSOURCE_HSI48);
  
  // The line of code below enables the peripheral clock for the hardware RNG.
  // Reference Manual, 9.8.17
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_RNG);
}

// ********************************************************************
static void periph_clocks_enable_crc(void)
{
  // *******************************************************
  // ********   CRC Clock setup                  ***********
  // *******************************************************

  // Enable AHB1 clock for CRC peripheral.
  // RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_CRC);

}

// ********************************************************************
static void BSP_periph_clocks_enable_(void)
{
  periph_clocks_enable_rng();  // RNG
  periph_clocks_enable_crc();  // CRC
}
  
// ********************************************************************
static void inline SystickInt_Enable(void)
{
  SysTick->CTRL  |= SysTick_CTRL_TICKINT_Msk;
}

// ********************************************************************
/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follows :
  *            System Clock source            = PLL (MSI)
  *            SYSCLK(Hz)                     = 110000000
  *            HCLK(Hz)                       = 110000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            MSI Frequency(Hz)              = 4000000
  *            PLL_M                          = 1
  *            PLL_N                          = 55
  *            PLL_R                          = 2
  *            Flash Latency(WS)              = 5
  *            Voltage range                  = 0
  * @retval None
  */
static void SystemClock_Config(void)
{
  /* Enable voltage range 0 mode for frequency above 80 Mhz */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
  LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE0);
  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_PWR);

  LL_FLASH_SetLatency(LL_FLASH_LATENCY_5);

  /* MSI already enabled at reset */

  /* Main PLL configuration and activation */
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_MSI, LL_RCC_PLLM_DIV_1, 55, LL_RCC_PLLR_DIV_2);
  LL_RCC_PLL_Enable();
  LL_RCC_PLL_EnableDomain_SYS();
  while(LL_RCC_PLL_IsReady() != 1)
  {
    /* sit and spin */
  }

  /* Sysclk activation on the main PLL */
  /* Intermediate AHB prescaler 2 when target frequency clock is higher than 80 MHz */
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_2);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
  {
    /* sit and spin */
  }

  /* Ensure 1�s transition state at intermediate medium speed clock based on DWT */
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  DWT->CYCCNT = 0;
  while(DWT->CYCCNT < 100)
  {
    /* sit and spin */
  }

  /* AHB prescaler 1 */
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  /* Set APB1 & APB2 prescaler*/
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);


  /* Update CMSIS variable (which can be updated also through SystemCoreClockUpdate function) */
  LL_SetSystemCoreClock(SYSTEM_CLOCK_FREQUENCY);

  // For commonly-used peripherals
  BSP_periph_clocks_enable_();
}

// ********************************************************************
void StartSystick(void)
{
  /* Set systick to 1ms in using frequency set to 110MHz */
  /* This frequency can be calculated through LL RCC macro */
  /* ex: __LL_RCC_CALC_PLLCLK_FREQ(MSI_VALUE,
                                  LL_RCC_PLLM_DIV_1, 55, LL_RCC_PLLR_DIV_2)*/
  LL_Init1msTick(HCLK_FREQUENCY);

  /* Enable systick interrupt */
  SystickInt_Enable();
  NVIC_EnableIRQ(SysTick_IRQn); 
}

// ********************************************************************
void BSP_board_init(void)
{
  // Configure the system clock to 110 MHz 
  SystemClock_Config();
  
  // Configure IO in output push-pull mode to drive external LED
  Configure_GPIO();
  
}

