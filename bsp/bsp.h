

#ifndef BSP_H
#define BSP_H

#include "stm32l5xx_ll_bus.h"
#include "stm32l5xx_ll_pwr.h"
#include "stm32l5xx_ll_rcc.h"
#include "stm32l5xx_ll_system.h"
#include "stm32l5xx_ll_utils.h"
#include "stm32l5xx_ll_gpio.h"


// ****************************************************
#define SYSTEM_CLOCK_FREQUENCY    (110U * 1000U * 1000U)
#define HCLK_FREQUENCY            SYSTEM_CLOCK_FREQUENCY


// ****************************************************
#define MY_ASSERT(condition) \
  do { \
    if ((condition) == 0) { \
      my_assert(); \
    } \
  } while(0)

#define BSP_TICKS_PER_SEC   (1000UL)

extern uint8_t user_button_pressed;
#define USER_BUTTON_PRESSED_DEBOUNCED()  (user_button_pressed)  // application should call this.

// ********************************************************************
extern void my_assert(void);
extern void BSP_board_init(void);
extern void BSP_led_on(int led_id);
extern void BSP_led_off(int led_id);
extern void BSP_led_toggle(int led_id);
extern void StartSystick(void);
extern int  UserButton_Pressed(void);   // application shouldn't call this.

// ********************************************************************


#endif  // BSP_H

