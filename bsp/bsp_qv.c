
#include "qpc.h"
#include "bsp.h"
#include "serial.h"
#include "signals.h"


#ifdef Q_SPY

QSTimeCtr QS_tickTime_;
QSTimeCtr QS_tickPeriod_;
static uint8_t const l_SysTick_Handler = 0U; // used for QSPY tracing
#endif

/* QS callbacks ============================================================*/
#ifdef Q_SPY

/*..........................................................................*/
uint8_t QS_onStartup(void const *arg) {

    static uint8_t qsBuf[2*1024]; /* buffer for Quantum Spy */

    (void)arg; /* avoid the "unused parameter" compiler warning */
    QS_initBuf(qsBuf, sizeof(qsBuf));

    // Enable serial port for board's USB Virtual Comm Port
    SerialInit();

    QS_tickPeriod_ = SystemCoreClock / BSP_TICKS_PER_SEC;
    QS_tickTime_ = QS_tickPeriod_; /* to start the timestamp at zero */

    /* setup the QS filters... */
    QS_FILTER_ON(QS_SM_RECORDS);
    QS_FILTER_ON(QS_UA_RECORDS);

    return (uint8_t)1; /* return success */
}

/*..........................................................................*/
void QS_onCleanup(void) {
  // Currently nothing
}

/*..........................................................................*/
QSTimeCtr QS_onGetTime(void) { /* NOTE: invoked with interrupts DISABLED */
    if ((SysTick->CTRL & 0x00010000) == 0) {  /* COUNT not set? */
        return QS_tickTime_ - (QSTimeCtr)SysTick->VAL;
    }
    else { /* the rollover occured, but the SysTick_ISR did not run yet */
        return QS_tickTime_ + QS_tickPeriod_ - (QSTimeCtr)SysTick->VAL;
    }
}

/*..........................................................................*/
void QS_onFlush(void) {

    uint16_t b;
    QF_INT_DISABLE();
    while ((b = QS_getByte()) != QS_EOD) {    /* while not End-Of-Data... */
        QF_INT_ENABLE();
        while (!TxEmpty()) { /* while TXE not empty */
        }
        uint8_t ch = (b & 0xFFU);
        TxBuffer(&ch, 1UL);
        QF_INT_DISABLE();
    }
    QF_INT_ENABLE();
}

/*..........................................................................*/
/*! callback function to reset the target (to be implemented in the BSP) */
void QS_onReset(void) {
    NVIC_SystemReset();
}

/*..........................................................................*/
/*! callback function to execute a user command (to be implemented in BSP) */
void QS_onCommand(uint8_t cmdId,
                  uint32_t param1, uint32_t param2, uint32_t param3)
{
  // USER PUTS OWN CODE HERE

    void assert_failed(char const *module, int loc);
    (void)cmdId;
    (void)param1;
    (void)param2;
    (void)param3;

#if 0
    QS_BEGIN_ID(COMMAND_STAT, 0U) /* app-specific record */
        QS_U8(2, cmdId);
        QS_U32(8, param1);
        QS_U32(8, param2);
        QS_U32(8, param3);
    QS_END()

    if (cmdId == 10U) {
        Q_ERROR();
    }
    else if (cmdId == 11U) {
        assert_failed("QS_onCommand", 123);
    }
#endif
}


#endif /* Q_SPY */


/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* Assign a priority to EVERY ISR explicitly by calling NVIC_SetPriority().
* DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
*/
enum KernelUnawareISRs { /* see NOTE00 */
    /* ... */
    MAX_KERNEL_UNAWARE_CMSIS_PRI  /* keep always last */
};

/* "kernel-unaware" interrupts can't overlap "kernel-aware" interrupts */
Q_ASSERT_COMPILE(MAX_KERNEL_UNAWARE_CMSIS_PRI <= QF_AWARE_ISR_CMSIS_PRI);

enum KernelAwareISRs {
    SYSTICK_PRIO = QF_AWARE_ISR_CMSIS_PRI, /* see NOTE00 */
    /* ... */
    MAX_KERNEL_AWARE_CMSIS_PRI /* keep always last */
};

/* "kernel-aware" interrupts should not overlap the PendSV priority */
Q_ASSERT_COMPILE(MAX_KERNEL_AWARE_CMSIS_PRI <= (0xFF >>(8-__NVIC_PRIO_BITS)));

/*..........................................................................*/
uint8_t user_button_pressed = 0;
void DebounceButtons(void) {
  static const QEvt ev_pressed = { BUTTON_PRESS_SIG, 0, 0 };
  static const QEvt ev_released = { BUTTON_RELEASE_SIG, 0, 0 };
  static uint16_t b1_state = 0;
  b1_state <<= 1;
  if (UserButton_Pressed()) {
    b1_state |= 1;
  }
  if (!user_button_pressed && (b1_state == 0xFFFF)) {
    user_button_pressed = 1;
    QF_PUBLISH(&ev_pressed, (void *)0);
  }
  else if (user_button_pressed && (b1_state == 0)) {
    user_button_pressed = 0;
    QF_PUBLISH(&ev_released, (void *)0);
  }
}

/*..........................................................................*/
/* ISRs used in this project ===============================================*/
void SysTick_Handler(void) {
#ifdef Q_SPY
    {
        // volatile uint32_t tmp = SysTick->CTRL; /* clear CTRL_COUNTFLAG */
        (void)SysTick->CTRL; /* clear CTRL_COUNTFLAG */
        QS_tickTime_ += QS_tickPeriod_; /* account for the clock rollover */
    }
#endif
    QF_TICK_X(0U, (void *)&l_SysTick_Handler); /* process time events for rate 0 */
    DebounceButtons();
}

/* BSP functions ===========================================================*/
void QP_BSP_init(void) {
    /* configure the FPU usage by choosing one of the options...
    *
    * Do NOT to use the automatic FPU state preservation and
    * do NOT to use the FPU lazy stacking.
    *
    * NOTE:
    * Use the following setting when FPU is used only by active objects
    * and NOT in any ISR. This setting is very efficient, but if any ISRs
    * start using the FPU, this can lead to corruption of the FPU registers.
    */
    FPU->FPCCR &= ~((1U << FPU_FPCCR_ASPEN_Pos) | (1U << FPU_FPCCR_LSPEN_Pos));
}

/* QF callbacks ============================================================*/
void QF_onStartup(void) {

#if 0
    /* set up the SysTick timer to fire at BSP_TICKS_PER_SEC rate */
    SysTick_Config(SystemCoreClock / BSP_TICKS_PER_SEC);
#endif
    StartSystick();

    /* assing all priority bits for preemption-prio. and none to sub-prio. */
    NVIC_SetPriorityGrouping(0U);

    /* set priorities of ALL ISRs used in the system, see NOTE00
    *
    * !!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    * Assign a priority to EVERY ISR explicitly by calling NVIC_SetPriority().
    * DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
    */
    NVIC_SetPriority(SysTick_IRQn,   SYSTICK_PRIO);
    /* ... */

    // Enable other interrupts here
}

/*..........................................................................*/
void QF_onCleanup(void) {
}

/*..........................................................................*/
void QV_onIdle(void) { /* CAUTION: called with interrupts DISABLED, NOTE01 */
    /* toggle LED1 on and then off, see NOTE02 */
        while (!TxEmpty()) { } /* while TXE not empty */

#ifdef Q_SPY
    QF_INT_ENABLE();

    QS_rxParse();  /* parse all newly-received bytes */

    if (TxEmpty()) {  /* is TXE empty? */
        uint16_t b;

        QF_INT_DISABLE();
        b = QS_getByte();
        QF_INT_ENABLE();

        if (b != QS_EOD) {  /* not End-Of-Data? */
            uint8_t ch = (b & 0xFFU);
            TxBuffer(&ch, 1UL);
        }
    }
#elif defined(NDEBUG)
    /* Put the CPU and peripherals to the low-power mode.
    * you might need to customize the clock management for your application,
    * see the datasheet for your particular Cortex-M MCU.
    */
    QV_CPU_SLEEP();  /* atomically go to sleep and enable interrupts */
#else
    QF_INT_ENABLE(); /* just enable interrupts */
#endif
}

/*..........................................................................*/
void Q_onAssert(char const *module, int loc) {
    /*
    * NOTE: add here your application-specific error handling
    */
    (void)module;
    (void)loc;
    QS_ASSERTION(module, loc, (uint32_t)10000U); /* report assertion to QS */
    NVIC_SystemReset();
}


/*****************************************************************************
* NOTE00:
* The QF_AWARE_ISR_CMSIS_PRI constant from the QF port specifies the highest
* ISR priority that is disabled by the QF framework. The value is suitable
* for the NVIC_SetPriority() CMSIS function.
*
* Only ISRs prioritized at or below the QF_AWARE_ISR_CMSIS_PRI level (i.e.,
* with the numerical values of priorities equal or higher than
* QF_AWARE_ISR_CMSIS_PRI) are allowed to call any QF services. These ISRs
* are "QF-aware".
*
* Conversely, any ISRs prioritized above the QF_AWARE_ISR_CMSIS_PRI priority
* level (i.e., with the numerical values of priorities less than
* QF_AWARE_ISR_CMSIS_PRI) are never disabled and are not aware of the kernel.
* Such "QF-unaware" ISRs cannot call any QF services. The only mechanism
* by which a "QF-unaware" ISR can communicate with the QF framework is by
* triggering a "QF-aware" ISR, which can post/publish events.
*
* NOTE01:
* The QV_onIdle() callback is called with interrupts disabled, because the
* determination of the idle condition might change by any interrupt posting
* an event. QV_onIdle() must internally enable interrupts, ideally
* atomically with putting the CPU to the power-saving mode.
*
* NOTE02:
* One of the LEDs is used to visualize the idle loop activity. The brightness
* of the LED is proportional to the frequency of invcations of the idle loop.
* Please note that the LED is toggled with interrupts locked, so no interrupt
* execution time contributes to the brightness of the User LED.
*/
