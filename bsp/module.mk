# Time-stamp: <2021-05-02 17:50:52 daniel>
# bsp/module.mk

# add source files to list of all source
src += \
 bsp/bsp.c \
 bsp/serial.c \
 bsp/bsp_qv.c

# Add to the list of include file directories
inc += -Ibsp
