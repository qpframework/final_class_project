
#include "serial.h"
#include "qpc.h"

#include "stm32l5xx_ll_lpuart.h"

// ***************************************************************
static void LPUART1_UART_Init(void)
{
  LL_LPUART_InitTypeDef LPUART_InitStruct = {0};
  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  // Set UART clock source
  LL_RCC_SetLPUARTClockSource(LL_RCC_LPUART1_CLKSOURCE_PCLK1);

  // Peripheral clock enable
  LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_LPUART1);
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOG);
  LL_PWR_EnableVddIO2();

  // LPUART1 GPIO Configuration
  // PG7   ------> LPUART1_TX
  // PG8   ------> LPUART1_RX
  GPIO_InitStruct.Pin = LL_GPIO_PIN_7|LL_GPIO_PIN_8;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_8;
  LL_GPIO_Init(GPIOG, &GPIO_InitStruct);

#if 0
  // LPUART1 interrupt Init
  NVIC_SetPriority(LPUART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
  NVIC_EnableIRQ(LPUART1_IRQn);
#endif

  LPUART_InitStruct.BaudRate = 115200;
  LPUART_InitStruct.DataWidth = LL_LPUART_DATAWIDTH_8B;
  LPUART_InitStruct.StopBits = LL_LPUART_STOPBITS_1;
  LPUART_InitStruct.Parity = LL_LPUART_PARITY_NONE;
  LPUART_InitStruct.TransferDirection = LL_LPUART_DIRECTION_TX_RX;
  LPUART_InitStruct.HardwareFlowControl = LL_LPUART_HWCONTROL_NONE;
  LL_LPUART_Init(LPUART1, &LPUART_InitStruct);
  LL_LPUART_SetTXFIFOThreshold(LPUART1, LL_LPUART_FIFOTHRESHOLD_1_8);
  LL_LPUART_SetRXFIFOThreshold(LPUART1, LL_LPUART_FIFOTHRESHOLD_1_8);
  LL_LPUART_Enable(LPUART1);

}

#define NVIC_PRIORITYGROUP_3    ((uint32_t)0x00000004)

// ***************************************************************
void SerialInit(void)
{


/* move*/   LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
/* move*/   LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

/* move*/   NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_3);

  // Disable the internal Pull-Up in Dead Battery pins of UCPD peripheral
  LL_PWR_DisableUCPDDeadBattery();

  LPUART1_UART_Init();

  /* Polling LPUART initialisation */
  while ((!(LL_LPUART_IsActiveFlag_TEACK(LPUART1))) || (!(LL_LPUART_IsActiveFlag_REACK(LPUART1))))
  {
  }

}

#if 0
// ***************************************************************
static void TxByte(uint8_t data)
{
}
#endif

// ***************************************************************
void LPUART1_rx_isr(void)
{
#ifdef Q_SPY
  uint8_t byte = 0x99;
  QS_RX_PUT(byte);
#endif
}

// ***************************************************************
void TxBuffer(uint8_t const *data, uint32_t len)
{
  while(len)
  {
    // Wait for Tx Empty (TXE)
    while (!LL_LPUART_IsActiveFlag_TXE(LPUART1)) { }

    // If last char to send, clear TC flag
    if (len == 1)
    {
      LL_LPUART_ClearFlag_TC(LPUART1);
    }

    // Write character to Transmit Data register.
    // TXE flag is cleared by writing data in TDR register
    LL_LPUART_TransmitData8(LPUART1, *data);

    --len;
  }

  // Wait for TC flag to be raised for last char.
  // This happens after TXE, it happens when last byte
  //  has been fully clocked/shifted out the USART and
  //  the USART is idle again.
  while (!LL_LPUART_IsActiveFlag_TC(LPUART1)) { }
}

// ***************************************************************
void TrySerialLoopback(void)
{
  if (LL_LPUART_IsActiveFlag_RXNE_RXFNE(LPUART1)) {
    unsigned char c = LL_LPUART_ReceiveData8(LPUART1);
    unsigned char errs = LL_LPUART_IsActiveFlag_FE(LPUART1);
    errs |= LL_LPUART_IsActiveFlag_NE(LPUART1);
    errs |= LL_LPUART_IsActiveFlag_PE(LPUART1);
    errs |= LL_LPUART_IsActiveFlag_ORE(LPUART1);
    if (!errs) {
      TxBuffer(&c, 1UL);
    }
    LL_LPUART_ClearFlag_FE(LPUART1);
    LL_LPUART_ClearFlag_NE(LPUART1);
    LL_LPUART_ClearFlag_PE(LPUART1);
    LL_LPUART_ClearFlag_ORE(LPUART1);
  }
}
