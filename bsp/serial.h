

#ifndef SERIAL_H
#define SERIAL_H

#include "bsp.h"
#include "stm32l5xx_ll_lpuart.h"

// ********************************************************************
extern void SerialInit(void);
extern void TxBuffer(uint8_t const *data, uint32_t len);
extern void TrySerialLoopback(void);

// ********************************************************************
static inline int TxEmpty(void) 
{
  return LL_LPUART_IsActiveFlag_TXE(LPUART1);
}

// ********************************************************************
static inline int TxIdle(void) 
{
  return LL_LPUART_IsActiveFlag_TC(LPUART1);
}

// ********************************************************************

#endif  // SERIAL_H

