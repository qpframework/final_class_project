# Time-stamp: <2021-05-03 10:25:47 daniel>
# qpc.mk

## ** NOTE **
## set this to the directory where you have qpc installed
qpc := /home/pseudo/dev/qp/qpc
qf := $(qpc)/src/qf
qv := $(qpc)/src/qv


src += \
 $(qf)/qep_hsm.c \
 $(qf)/qep_msm.c \
 $(qf)/qf_act.c \
 $(qf)/qf_actq.c \
 $(qf)/qf_defer.c \
 $(qf)/qf_dyn.c \
 $(qf)/qf_mem.c \
 $(qf)/qf_ps.c \
 $(qf)/qf_qact.c \
 $(qf)/qf_qeq.c \
 $(qf)/qf_qmact.c \
 $(qf)/qf_time.c \
 $(qv)/qv.c


# add QPC directories to list of headers
inc += \
 -I$(qpc)/include \
 -I$(qpc)/src
