
#ifndef APP_TRACE_H
#define APP_TRACE_H

#include "qpc.h"

Q_DEFINE_THIS_FILE

#ifdef Q_SPY

// application-specific trace records
enum AppRecords {
  USER_BUTTON_PRESS = QS_USER,
  USER_BUTTON_RELEASE,
  LED1_ON = QS_USER,
  LED1_OFF,
  LED2_ON,
  LED2_OFF,
  LED3_ON,
  LED3_OFF
};

#endif

// ***************************************************
static inline void do_qs_init(void) {
  // initialize the QS software tracing...
  if (QS_INIT((void *)0) == 0U) {
    Q_ERROR();
  }
  QS_USR_DICTIONARY(USER_BUTTON_PRESS);
  QS_USR_DICTIONARY(USER_BUTTON_RELEASE);
  QS_USR_DICTIONARY(LED1_ON);
  QS_USR_DICTIONARY(LED1_OFF);
  QS_USR_DICTIONARY(LED2_ON);
  QS_USR_DICTIONARY(LED2_OFF);
  QS_USR_DICTIONARY(LED3_ON);
  QS_USR_DICTIONARY(LED3_OFF);
}

#ifdef Q_SPY
static uint8_t led_1_id;
#endif

static inline void apptrace_led1_on(void) {
  QS_BEGIN(LED1_ON, &led_1_id)   // application-specific record begin
    QS_U8(1, 1);
  QS_END()
}

static inline void apptrace_led1_off(void) {
  QS_BEGIN(LED1_OFF, &led_1_id)   // application-specific record begin
    QS_U8(1, 0);
  QS_END()
}


#endif  // APP_TRACE_H

