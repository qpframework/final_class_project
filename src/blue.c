// Time-stamp: <2021-05-03 19:51:51 daniel>
// src/blue.c


// Header Files
//-----------------------------------------------------------------------------
#include "app_trace.h"  // defined Q_this_module
#include "led.h"
#include "blue.h"
#include "signals.h"


// Active Object
//-----------------------------------------------------------------------------
static led_t Blue;  // single instance of the blue led object
QActive* const Blue_Super = &Blue.super;


// Queue for this active object
//-----------------------------------------------------------------------------
static QEvt const* Blue_queue[ QUEUE_DEPTH ];


// Initialize the blue led active object
//-----------------------------------------------------------------------------
static QState blue_init( led_t* const me, void const* const par ) {

  (void)par;  // compiler complains about unused paramters
  return Q_TRAN( &led_off );

};  // end blue_init


// Blue Active Object constructor
//-----------------------------------------------------------------------------
void blue_ctor( ) {

  Blue.color = BLUE;
  QActive_ctor( Blue_Super, Q_STATE_CAST( &blue_init ) );

  QACTIVE_START(
                Blue_Super,
                PRIORITY + BLUE,
                Blue_queue,
                Q_DIM( Blue_queue ),
                (void*)0,
                0U,
                (QEvt*)0
                );  // end qactive start

  QActive_subscribe( Blue_Super, FLIP_SIG );

};  // end blue_ctor
