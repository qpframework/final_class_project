// Time-stamp: <2021-05-03 15:09:32 daniel>
// src/blue.h
#ifndef __BLUE_H__
#define __BLUE_H__


// Header Files
//-----------------------------------------------------------------------------
#include "led.h"


// Macro
//-----------------------------------------------------------------------------
#define BLUE_PRIO LED_PRIO + BLUE


// Blue Active Object interface
//-----------------------------------------------------------------------------
void blue_ctor( );  // prototype


#endif  // __BLUE_H__
