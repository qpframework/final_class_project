// Time-stamp: <2021-05-03 20:59:21 daniel>
// src/bouncer.c


// Header Files
//-----------------------------------------------------------------------------
#include <time.h>
#include "bouncer.h"
#include "signals.h"
#include "common.h"
#include "bsp.h"
#include "app_trace.h"  // define Q_this_module_


// Bouncer states prototypes
//-----------------------------------------------------------------------------
static QState bouncer_init( bouncer_t* const, void const* const );
static QState bouncer_state( bouncer_t* const, QEvt const* const );


// Active Object
//-----------------------------------------------------------------------------
static bouncer_t Debouncer;  // single instance of the Debouncer object
QActive* const Debouncer_Super = &Debouncer.super;


// Queue for this active object
//-----------------------------------------------------------------------------
static QEvt const* Debouncer_queue[ QUEUE_DEPTH ];


// Flip Event
//-----------------------------------------------------------------------------
static QEvt Flip_Event = { FLIP_SIG, 0U, 0U };
static QEvt ShortPress_Event = { SHORT_PRESS_SIG, 0U, 0U };
static QEvt LongPress_Event = { LONG_PRESS_SIG, 0U, 0U };


// The bouncer object constructor
//-----------------------------------------------------------------------------
static void bouncer_ctor( ) {

  Debouncer.period = FOUR_HUNDRED_MS;
  QActive_ctor( Debouncer_Super, Q_STATE_CAST( &bouncer_init ) );
  QTimeEvt_ctorX( &Debouncer.time_event, Debouncer_Super, TIMEOUT_SIG, 0U );

  QACTIVE_START(
                Debouncer_Super,
                BOUNCER_PRIO,
                Debouncer_queue,
                Q_DIM( Debouncer_queue ),
                (void*)0,
                0U,
                (QEvt*)0
                );  // end qactive start

  QActive_subscribe( Debouncer_Super, BUTTON_PRESS_SIG );
  QActive_subscribe( Debouncer_Super, BUTTON_RELEASE_SIG );
  QActive_subscribe( Debouncer_Super, SHORT_PRESS_SIG );
  QActive_subscribe( Debouncer_Super, LONG_PRESS_SIG );

};  // end bouncer_ctor


//
//-----------------------------------------------------------------------------
void bouncer_start( ) {

  bouncer_ctor();

};  // end bouncer_start


// The initial state for the bouncer object
//-----------------------------------------------------------------------------
static QState bouncer_init( bouncer_t* const me, void const* const par ) {

  (void)par;  // compiler warns about unused parameters
  QTimeEvt_armX( &me->time_event, me->period, me->period );
  return Q_TRAN( &bouncer_state );

};  // end bouncer_init


// When the button is released calculate if it was long or short
//-----------------------------------------------------------------------------
void button_released( bouncer_t* const me ) {

    time_t duration = time(0) - me->press_time;
    if ( duration > 79 && duration < 271 )
      QF_PUBLISH( &ShortPress_Event, (void*)0 );
    if ( duration > 503 && duration < 997 )
      QF_PUBLISH( &LongPress_Event, (void*)0 );

};  // end button_released


// Adjust the rate of blinking
//-----------------------------------------------------------------------------
void adjust_period( bouncer_t* const me, uint32_t period ) {

  if ( period < 50 ) period = FOUR_HUNDRED_MS;
  me->period = period;  // save current period
  QTimeEvt_disarm( &me->time_event );
  QTimeEvt_armX( &me->time_event, me->period, me->period );

};  // end adjust_period


// The Debounce object has two states: up and down
//-----------------------------------------------------------------------------
static QState bouncer_state( bouncer_t* const me, QEvt const* const e ) {

  QState status_ = Q_HANDLED();

  switch( e->sig ) {

  case TIMEOUT_SIG:
    QF_PUBLISH( &Flip_Event, (void*)0 );
    break;

  case BUTTON_PRESS_SIG:
    me->press_time = time(0);  // get system time
    break;

  case BUTTON_RELEASE_SIG:
    button_released( me );
    break;

  case LONG_PRESS_SIG:
    adjust_period( me, me->period / 2 );
    break;

  case SHORT_PRESS_SIG:
    adjust_period( me, FOUR_HUNDRED_MS );
    break;

  default:
    status_ = Q_SUPER( &QHsm_top );
    break;

  };  // end switch

  return status_;

};  // end bouncer_state
