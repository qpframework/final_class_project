// Time-stamp: <2021-05-03 20:47:22 daniel>
// src/bouncer.h

#ifndef __BOUNCER_H__
#define __BOUNCER_H__

// Header Files
//-----------------------------------------------------------------------------
#include <time.h>  // for system time
#include "qpc.h"  // QP object


// MACROS used with the Debouncer object
//-----------------------------------------------------------------------------
#define FOUR_HUNDRED_MS 400UL
#define BOUNCER_PRIO 1


// The Debouncer class
//-----------------------------------------------------------------------------
typedef struct {

  QActive super;  // parent object
  QTimeEvt time_event;  // private member holding information on the timer
  uint32_t period;  // how long will each timeout be
  time_t press_time;  // when did the button press signal arrive

} bouncer_t;  // class name


// Public interface
//-----------------------------------------------------------------------------
void bouncer_start( );
extern QActive* const Debouncer_Super;


#endif  // __BOUNCER_H__
