// Time-stamp: <2021-05-03 19:57:36 daniel>
// src/common.h

#ifndef __COMMON_H__
#define __COMNON_H__

// Shared Macros
//-----------------------------------------------------------------------------
#define QUEUE_DEPTH 10

// Colors
#define RED 3
#define BLUE 2
#define GREEN 1

// Prioity
#define PRIORITY 1

#endif  // __COMMON_H__
