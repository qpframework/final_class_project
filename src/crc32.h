
#ifndef CRC32_H
#define CRC32_H

/** \file crc32.h
*
* @brief Embedded Security Boot Camp
*
* @par
* COPYRIGHT NOTICE: (c) Ontera Corporation
* All rights reserved.
*/

#include <stdint.h>

extern uint32_t CRC_CalcSW(uint32_t const *DataBuffer, uint32_t BuffSize);


#endif  // CRC32_H
