// Time-stamp: <2021-05-03 19:54:13 daniel>
// src/green.c


// Header Files
//-----------------------------------------------------------------------------
#include "app_trace.h"  // defined Q_this_module
#include "led.h"
#include "green.h"
#include "signals.h"


// Active Object
//-----------------------------------------------------------------------------
static led_t Green;  // single instance of the green led object
QActive* const Green_Super = &Green.super;


// Queue for this active object
//-----------------------------------------------------------------------------
static QEvt const* Green_queue[ QUEUE_DEPTH ];


// Green Active Object constructor
//-----------------------------------------------------------------------------
void green_ctor( ) {

  Green.color = GREEN;
  QActive_ctor( Green_Super, Q_STATE_CAST( &led_init ) );

  QACTIVE_START(
                Green_Super,
                PRIORITY + GREEN,
                Green_queue,
                Q_DIM( Green_queue ),
                (void*)0,
                0U,
                (QEvt*)0
                );  // end qactive start

  QActive_subscribe( Green_Super, FLIP_SIG );

};  // end green_ctor
