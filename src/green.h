// Time-stamp: <2021-05-03 19:54:52 daniel>
// src/green.h
#ifndef __GREEN_H__
#define __GREEN_H__


// Header Files
//-----------------------------------------------------------------------------
#include "led.h"


// Green Active Object interface
//-----------------------------------------------------------------------------
void green_ctor( );  // prototype


#endif  // __GREEN_H__
