// Time-stamp: <2021-05-03 19:56:55 daniel>
// src/led.c

// Header Files
//-----------------------------------------------------------------------------
#include "led.h"
#include "app_trace.h"
#include "bsp.h"
#include "signals.h"
#include "red.h"
#include "green.h"
#include "blue.h"


// Initial state for LEDs
//-----------------------------------------------------------------------------
QState led_init( led_t* const me, void const* const par ) {

  (void)par;  // compiler complains if a parameter is not used
  return Q_TRAN( &led_on );

};  // end led_init


// SM state to turn on the LED
//-----------------------------------------------------------------------------
QState led_on( led_t* const me, QEvt const* const e ) {

  QState status_;

  switch ( e->sig ) {

  case Q_ENTRY_SIG:
    BSP_led_on( me->color );
    status_ = Q_HANDLED();
    break;

  case FLIP_SIG:
    status_ = Q_TRAN( &led_off );
    break;

  default:
    status_ = Q_SUPER( &QHsm_top );
    break;

  };  // end switch

  return status_;

};  // end led_up


// SM state to turn off the LED
//-----------------------------------------------------------------------------
QState led_off( led_t* const me, QEvt const* const e ) {

  QState status_;

  switch (e->sig) {

  case Q_ENTRY_SIG:
    BSP_led_off( me->color );
    status_ = Q_HANDLED();
    break;

  case FLIP_SIG:
    status_ = Q_TRAN( &led_on );
    break;

  default:
    status_ = Q_SUPER( &QHsm_top );
    break;

  };  // end switch

  return status_;

};  // end led_up




void led_start( ) {

  green_ctor();
  red_ctor();
  blue_ctor();

};  // end led_start
