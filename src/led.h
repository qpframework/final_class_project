// Time-stamp: <2021-05-03 19:52:21 daniel>
// src/led.h
#ifndef __LED_H__
#define __LED_H__


// Header Files
//-----------------------------------------------------------------------------
#include "qpc.h"
#include "common.h"


// LED Macros
//-----------------------------------------------------------------------------
#define LED_PRIO 1


// Definition of an LED object
//-----------------------------------------------------------------------------
typedef struct {

  QActive super;  // parent object
  uint32_t color;  // what color is this LED

} led_t;  // type for an led


// LED state prototypes
//-----------------------------------------------------------------------------
QState led_init( led_t* const, void const* const );
QState led_on( led_t* const, QEvt const* const );
QState led_off( led_t* const, QEvt const* const );


void led_start();


#endif  // __LED_H__
