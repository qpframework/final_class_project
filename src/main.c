// Time-stamp: <2021-05-03 20:00:37 daniel>
// src/main.c


// Header Files
//-----------------------------------------------------------------------------
#include "bsp.h"
#include "qpc.h"
#include "bouncer.h"
#include "led.h"
#include "blue.h"
#include "app_trace.h"
#include "pubsub.h"
#include "pools.h"

// ****************************************************************
//  Main program
// ****************************************************************

extern void QP_BSP_init(void);

//-----------------------------------------------------------------------------
int main() {

    // initialize the framework and the underlying RT kernel
    QF_init();

    // Init board
    BSP_board_init();
    QP_BSP_init();


    // Initialize publish-subscribe system (optional -- not required to use)
    init_pub_sub();
    // Initialize dynamic even pool system (optional -- not required to use)
    init_pools();  //

    //start_application();
    bouncer_start();  // start the main active object
    led_start();

    return QF_run();  // start the QP framework

};  // end main
