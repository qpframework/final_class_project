# Time-stamp: <2021-05-03 19:53:15 daniel>
# src/module.mk

# add additional source file
src += \
 src/bouncer.c \
 src/led.c \
 src/blue.c \
 src/red.c \
 src/green.c \
 src/main.c \
 src/pools.c \
 src/pubsub.c \
 src/time_stamp.c

# add additional directories to the inc directories
inc += -Isrc
