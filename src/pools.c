
#include "qpc.h"
#include "pools.h"

// *******************************************************************
// POOL 1 - small events
//  Adjust size & count to your application's needs
// *******************************************************************
typedef  uint8_t SMALL_EV[12] ;
#define  POOL1_NUM_EVENTS   (10)
static QF_MPOOL_EL(SMALL_EV) Pool_1[POOL1_NUM_EVENTS];

static inline void createPool1(void) {
    QF_poolInit(Pool_1, sizeof(Pool_1), sizeof(Pool_1[0]));
}

#if 0
// *******************************************************************
// POOL 2 - medium events
//  Adjust size & count to your application's needs
// *******************************************************************
typedef  uint8_t small_array[20] MED_EV;
#define  POOL2_NUM_EVENTS   (8)
static QF_MPOOL_EL(MED_EV) Pool_2[POOL2_NUM_EVENTS];

static inline void createPool2(void) {
    QF_poolInit(Pool_2, sizeof(Pool_2), sizeof(Pool_2[0]));
}
#endif

//******************************************************
void init_pools(void) {
    createPool1();
#if 0
    createPool2();
#endif
}
