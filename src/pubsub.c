// time-stamp: <>
// pubsub.c

// Header Files
//-----------------------------------------------------------------------------
#include "qpc.h"
#include "signals.h"  // for MAX_PUB_SIG
#include "pubsub.h"

static QSubscrList subscriber_list[ MAX_PUB_SIG ];


// Initialize publish / subscribe
//-----------------------------------------------------------------------------
void init_pub_sub( void ) {

    QF_psInit( subscriber_list, Q_DIM( subscriber_list ) );

};  // end init_pub_sub
