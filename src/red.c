// Time-stamp: <2021-05-03 19:51:19 daniel>
// src/red.c


// Header Files
//-----------------------------------------------------------------------------
#include "app_trace.h"  // defined Q_this_module
#include "led.h"
#include "red.h"
#include "signals.h"


// Active Object
//-----------------------------------------------------------------------------
static led_t Red;  // single instance of the red led object
QActive* const Red_Super = &Red.super;


// Queue for this active object
//-----------------------------------------------------------------------------
static QEvt const* Red_queue[ QUEUE_DEPTH ];




// Red Active Object constructor
//-----------------------------------------------------------------------------
void red_ctor( ) {

  Red.color = RED;
  QActive_ctor( Red_Super, Q_STATE_CAST( &led_init ) );

  QACTIVE_START(
                Red_Super,
                PRIORITY + RED,
                Red_queue,
                Q_DIM( Red_queue ),
                (void*)0,
                0U,
                (QEvt*)0
                );  // end qactive start

  QActive_subscribe( Red_Super, FLIP_SIG );

};  // end red_ctor
