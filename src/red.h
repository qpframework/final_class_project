// Time-stamp: <2021-05-03 19:55:12 daniel>
// src/red.h
#ifndef __RED_H__
#define __RED_H__


// Header Files
//-----------------------------------------------------------------------------
#include "led.h"


// Red Active Object interface
//-----------------------------------------------------------------------------
void red_ctor( );  // prototype


#endif  // __RED_H__
