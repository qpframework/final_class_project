// Time-stamp: <2021-05-03 20:35:29 daniel>
// src/signals.h

#ifndef SIGNALS_H
#define SIGNALS_H


// Defined signals used in this project
//-----------------------------------------------------------------------------
enum project_signals {

  DUMMY_SIG = Q_USER_SIG,
  BUTTON_PRESS_SIG,
  BUTTON_RELEASE_SIG,
  FLIP_SIG,             // signal an LED to flip state on or off
  LONG_PRESS_SIG,       // Simulated long press of the button
  SHORT_PRESS_SIG,      // Simulated short press of the button
  MAX_PUB_SIG,          // Last published signal

  TIMEOUT_SIG,
  MAX_SIG               // Very last signal

};  // end project_signals


#endif /* SIGNALS_H */
