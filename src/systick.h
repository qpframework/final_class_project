
#ifndef SYSTICK_H
#define SYSTICK_H

#include <stdint.h>

// SYSTICK registers
#define SYSTICK_CSR     (*((uint32_t volatile *)0xE000E010))
#define SYSTICK_RELOAD  (*((uint32_t volatile *)0xE000E014))
#define SYSTICK_CURRENT (*((uint32_t volatile *)0xE000E018))


// ****************************************************************
// Init ARM SYSTICK timer & start it counting down
// ****************************************************************
#define INIT_SYSTICK(ST_PERIOD) \
do \
{ \
  SYSTICK_CSR = 0;                         \
  SYSTICK_RELOAD = ST_PERIOD;              \
  SYSTICK_CURRENT = 0;                     \
  SYSTICK_CSR = ((1UL << 2) | (1UL << 0)); \
} while (0)

// ****************************************************************
// SYSTICK counts *down*, so in the normal case, start time > stop time.
// If stop time > start time, we "underflowed".
// This does not account for timing intervals longer than the
// SYSTICK reload period.
// uint32_t calcSystickDelta(uint32_t startTime, uint32_t stopTime)
#define CALC_SYSTICK_DELTA(startTime, stopTime) \
    ((startTime > stopTime) ? (startTime - stopTime) : (SYSTICK_RELOAD - (stopTime - startTime)))


#endif  // SYSTICK_H

