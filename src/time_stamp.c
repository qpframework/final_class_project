// Time-stamp: <2021-05-02 19:22:00 daniel>
// src/time_stamp.c


// Header Files
//-----------------------------------------------------------------------------
#include "time_stamp.h"


// Hold date and time of this build
//-----------------------------------------------------------------------------
char const build_date[ DATE_LENGTH ] = __DATE__;
char const build_time[ TIME_LENGTH ] = __TIME__;
