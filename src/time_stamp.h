// Time-stamp: <2021-05-02 19:24:27 daniel>
// src/time_stamp.h

#ifndef __TIME_STAMP_H__
#define __TIME_STAMP_H__


// Macros used with data / time
//-----------------------------------------------------------------------------
#define DATE_LENGTH 12
#define TIME_LENGTH 9


// External reference
//-----------------------------------------------------------------------------
extern char const build_date[ DATE_LENGTH ];
extern char const build_time[ TIME_LENGTH ];

#endif /* __TIME_STAMP_H__ */
