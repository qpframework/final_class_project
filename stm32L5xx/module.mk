# Time-stamp: <2021-05-02 15:55:27 daniel>
# stm32L5xx/module.mk

stm := stm32L5xx

src += \
 $(stm)/stm32l5xx_it.c \
 $(stm)/stm32l5xx_ll_gpio.c \
 $(stm)/stm32l5xx_ll_lpuart.c \
 $(stm)/stm32l5xx_ll_rcc.c \
 $(stm)/stm32l5xx_ll_utils.c \
 $(stm)/system_stm32l5xx.c

# Special case for the assembly code
objs += $(stm)/startup_stm32l552zetxq.o
