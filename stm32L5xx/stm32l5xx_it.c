/**
  ******************************************************************************
  * @file    Examples_LL/GPIO/GPIO_InfiniteLedToggling/Src/stm32l5xx_it.c
  * @author  MCD Application Team
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l5xx_it.h"

/******************************************************************************/
/*            Cortex-M33 Processor Exceptions Handlers                         */
/******************************************************************************/

/******************************************************************************/
static void SitAndSpin(void)
{
  SitAndSpin();
}

#if 0
/******************************************************************************/
void Reset_Handler(void)
{
  SystemInit();
}
#endif

// for QP projects, we've moved this to another file.
#if 0
/******************************************************************************/
void NMI_Handler(void)
{
  SitAndSpin();
}
#endif

/******************************************************************************/
void HardFault_Handler(void)
{
  SitAndSpin();
}

/******************************************************************************/
void MemManage_Handler(void)
{
  SitAndSpin();
}

/******************************************************************************/
void BusFault_Handler(void)
{
  SitAndSpin();
}

/******************************************************************************/
void UsageFault_Handler(void)
{
  SitAndSpin();
}

/******************************************************************************/
void SVC_Handler(void)
{
  SitAndSpin();
}

/******************************************************************************/
void DebugMon_Handler(void)
{
  SitAndSpin();
}

// for QP projects, we've moved the Systick_Handler()
// to another file.
#if 0
/******************************************************************************/
void PendSV_Handler(void)
{
  SitAndSpin();
}
#endif

// for QP projects, we've moved the Systick_Handler()
// to another file.
#if 0
/******************************************************************************/
void SysTick_Handler(void)
{
  SitAndSpin();
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
